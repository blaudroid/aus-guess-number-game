<?php
namespace Nastase\GuessNumber\Strategy;

use Nastase\GuessNumber\Services\RandomNumberGenerator;
use Nastase\GuessNumber\Services\Terminal;

class UserStrategy implements Strategy
{
    private int         $numberToBeGuessed;
    private Terminal    $terminal;

    public function __construct()
    {
        $this->terminal = new Terminal();
    }

    public function setNumberToGuess(int $number): void
    {
        $this->numberToBeGuessed = $number;
    }

    public function getRoundResults(): array
    {
        $userInput = $this->terminal->getInput();

        if($userInput > $this->numberToBeGuessed) {
            return [
                'status' => false,
                'message' => 'Your number was greater than the to be guessed number! Try again!'
            ];
        }

        if($userInput < $this->numberToBeGuessed) {
            return [
                'status' => false,
                'message' => 'Your number was smaller than the to be guessed number! Try again!'
            ];
        }

        return [
            'status' => true,
            'message' => ''
        ];
    }

    public function setRandomNumberGenerator(RandomNumberGenerator $generator): void { }
}
