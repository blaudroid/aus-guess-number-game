<?php

namespace Nastase\GuessNumber\Strategy;

use Nastase\GuessNumber\Services\RandomNumberGenerator;

interface Strategy
{
    public function setNumberToGuess(int $number): void;
    public function getRoundResults(): array;
    public function setRandomNumberGenerator(RandomNumberGenerator $generator): void;
}
