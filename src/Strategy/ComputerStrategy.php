<?php
namespace Nastase\GuessNumber\Strategy;

use Exception;
use Nastase\GuessNumber\Services\RandomNumberGenerator;

class ComputerStrategy implements Strategy
{
    private int                     $numberToBeGuessed;
    private array                   $alreadyUsedNumbers = [];

    private RandomNumberGenerator   $randomNumberGenerator;

    /**
     * @throws Exception
     */
    public function getComputerGuess(): string
    {
        $generatedNumber = $this->randomNumberGenerator->generateRandomNumber();

        if(!in_array($generatedNumber, $this->alreadyUsedNumbers, false)) {
            return $generatedNumber;
        }

        return $this->getComputerGuess();
    }

    public function setNumberToGuess(int $number): void
    {
        $this->numberToBeGuessed = $number;
    }

    /**
     * @throws Exception
     */
    public function getRoundResults(): array
    {
        $computerGuess = $this->getComputerGuess();

        if($computerGuess > $this->numberToBeGuessed) {
            $this->alreadyUsedNumbers[] = $computerGuess;

            $this->randomNumberGenerator->setRange(
                $this->randomNumberGenerator->getRangeMinValue(),
                $computerGuess
            );

            return [
                'status' => false,
                'message' => "Computer tried: $computerGuess"
            ];
        }

        if($computerGuess < $this->numberToBeGuessed) {
            $this->alreadyUsedNumbers[] = $computerGuess;

            $this->randomNumberGenerator->setRange(
                $computerGuess,
                $this->randomNumberGenerator->getRangeMaxValue()
            );

            return [
                'status' => false,
                'message' => "Computer tried: $computerGuess"
            ];
        }

        return [
            'status' => true,
            'message' => "Computer tried: $computerGuess"
        ];
    }

    public function setRandomNumberGenerator(RandomNumberGenerator $generator): void
    {
        $this->randomNumberGenerator = $generator;
    }
}
