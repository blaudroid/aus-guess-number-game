<?php

namespace Nastase\GuessNumber\Engine;

use Nastase\GuessNumber\Services\Terminal;

class GameGUI
{
    private Terminal    $terminal;
    private int         $menuSelection;
    private array       $finishPageDetails;
    private const       DELIMITER = '==============================';
    public const        START_GAME = 1;
    public const        CHOOSE_GAME_MODE = 2;
    public const        QUIT_GAME = 3;
    public const        MENU_BACK = 3;
    public const        INVALID_SELECTION = -1;
    public const        MENU_YES = 1;
    public const        MENU_NO = 2;

    public function __construct()
    {
        $this->terminal = new Terminal();
    }

    public function showMenu(): void
    {
        $this->printMenuHeader('GUESS NUMBER GAME');
        $this->terminal->print(Terminal::COLOR_YELLOW . '[1] START GAME');
        $this->terminal->print(Terminal::COLOR_YELLOW . '[2] CHOOSE GAME MODE');
        $this->terminal->print(Terminal::COLOR_YELLOW . '[3] QUIT GAME');
        $this->printDelimiter();

        $this->resetMenuSelection();
        $this->setMenuSelectedValue(3);

        if($this->menuSelection === self::INVALID_SELECTION) {
            self::showMenu();
        }
    }

    public function showSelectGameModeMenu(): void
    {
        $this->printMenuHeader('GNG: SELECT GAME MODE');
        $this->terminal->print(Terminal::COLOR_YELLOW . '[1] COMPUTER PLAYS THE GAME FOR YOU');
        $this->terminal->print(Terminal::COLOR_YELLOW . '[2] YOU PLAY THE GAME');
        $this->terminal->print(Terminal::COLOR_YELLOW . '[3] BACK');
        $this->printDelimiter();

        $this->resetMenuSelection();
        $this->setMenuSelectedValue(3);

        if($this->menuSelection === self::INVALID_SELECTION) {
            self::showSelectGameModeMenu();
        }
    }

    public function showEndGameResult(): void
    {
        $this->printDelimiter();
        $this->terminal->print(Terminal::COLOR_GREEN . 'Congratulations! You have finished the game!');
        $this->terminal->print(Terminal::COLOR_GREEN . 'Tries: ' . Terminal::COLOR_YELLOW . $this->finishPageDetails['triesCount']);
        $this->terminal->print(Terminal::COLOR_GREEN . 'Guessed number: ' . Terminal::COLOR_YELLOW . $this->finishPageDetails['numberGuessed']);
    }

    public function showPlayAgainMenu(): void
    {
        $this->printMenuHeader('GNG: PLAY AGAIN ?');
        $this->terminal->print(Terminal::COLOR_YELLOW . '[1] YES');
        $this->terminal->print(Terminal::COLOR_YELLOW . '[2] NO');
        $this->printDelimiter();

        $this->resetMenuSelection();
        $this->setMenuSelectedValue(2);

        if($this->menuSelection === self::INVALID_SELECTION) {
            self::showPlayAgainMenu();
        }
    }

    public function setMenuSelectedValue(int $maxOptions): void
    {
        $input = (int) $this->terminal->getInput();

        if($input > $maxOptions) {
            $this->printGameError('Invalid option selected!');
            $this->menuSelection = self::INVALID_SELECTION;
        } else {
            $this->menuSelection = $input;
        }
    }

    public function getMenuSelection(): int
    {
        return $this->menuSelection;
    }

    public function resetMenuSelection(): void
    {
        $this->menuSelection = self::INVALID_SELECTION;
    }

    public function setFinishPageDetails(array $finishPageDetails): void
    {
        $this->finishPageDetails = $finishPageDetails;
    }

    private function printMenuHeader(string $menuHeader): void
    {
        $this->printDelimiter();
        $this->terminal->print(Terminal::COLOR_GREEN . $menuHeader);
        $this->printDelimiter();
    }

    private function printDelimiter(bool $spacer = false): void
    {
        $out = $spacer ? ' ' . self::DELIMITER : self::DELIMITER;
        $this->terminal->print(Terminal::COLOR_BLUE . $out);
    }

    public function printGameError(string $message): void
    {
        $this->terminal->print(Terminal::COLOR_RED . 'Game error: ' . Terminal::COLOR_YELLOW . $message);
    }

    public function printEndGameMessage(): void
    {
        $this->printDelimiter();
        $this->terminal->print(Terminal::COLOR_GREEN . 'Thank you for playing! Have a nice day!');
        $this->printDelimiter();
    }

    public function printRoundMessage(string $message): void
    {
        $this->terminal->print(Terminal::COLOR_GREEN . $message);
    }

    public function printGuessNumberBetweenMessage(int $min, int $max): void
    {
        $this->printDelimiter();
        $this->terminal->print(Terminal::COLOR_GREEN . 'Guess the number between ' . Terminal::COLOR_RED . $min . Terminal::COLOR_GREEN . ' and ' . Terminal::COLOR_RED . $max . Terminal::COLOR_GREEN . ':');
        $this->printDelimiter();
    }
}
