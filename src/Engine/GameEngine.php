<?php

namespace Nastase\GuessNumber\Engine;

use Nastase\GuessNumber\Strategy\Strategy;

class GameEngine
{
    private int                     $triesCount = 1;
    private bool                    $roundStatus;
    private string                  $roundMessage;

    private Strategy                $strategy;

    public function setStrategy(Strategy $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function playRound(): void
    {
        $roundResults = $this->strategy->getRoundResults();

        if(!$roundResults['status']) {
            $this->increaseTriesCount();
        }

        $this->setRoundStatus($roundResults['status']);
        $this->setRoundMessage($roundResults['message']);
    }

    private function setRoundStatus(bool $status): void
    {
        $this->roundStatus = $status;
    }

    public function getRoundStatus(): bool
    {
        return $this->roundStatus;
    }

    private function setRoundMessage(string $message): void
    {
        $this->roundMessage = $message;
    }

    public function getRoundMessage(): string
    {
        return $this->roundMessage;
    }

    private function increaseTriesCount(): void
    {
        ++$this->triesCount;
    }

    public function getTriesCount(): int
    {
        return $this->triesCount;
    }

    public function resetGame(): void
    {
        $this->triesCount = 1;
    }
}
