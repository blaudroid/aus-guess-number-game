<?php

namespace Nastase\GuessNumber\Services;

use Exception;

class RandomNumberGenerator
{
    private int $min;
    private int $max;
    private int $originalMin;
    private int $originalMax;

    private bool $originalsSet = false;

    public function setRange(int $min, int $max): void
    {
        $this->min = $min;
        $this->max = $max;

        if(!$this->originalsSet) {
            $this->originalMax = $max;
            $this->originalMin = $min;
        }
    }

    /**
     * @throws Exception
     */
    public function generateRandomNumber(): int
    {
        return random_int($this->min, $this->max);
    }

    public function getRangeMinValue(): int
    {
        return $this->min;
    }

    public function getRangeMaxValue(): int
    {
        return $this->max;
    }

    public function saveOriginalRange(): void
    {
        $this->originalsSet = true;
    }

    public function getOriginalMin(): int
    {
        return $this->originalMin;
    }

    public function getOriginalMax(): int
    {
        return $this->originalMax;
    }
}
