<?php

namespace Nastase\GuessNumber\Services;

class Terminal
{
    public const COLOR_BLUE         = "\033[34m ";
    public const COLOR_DEFAULT      = "\033[39m ";
    public const COLOR_RED          = "\033[91m ";
    public const COLOR_GREEN        = "\033[92m ";
    public const COLOR_YELLOW       = "\033[93m ";
    public const COLOR_GREY         = "\033[37m ";

    public function getInput(): string
    {
        $input = readline();

        if(!$input) {
            $input = $this->getInput();
        }

        return $input;
    }

    public function print(string $message): void
    {
        echo $message . PHP_EOL . self::COLOR_DEFAULT;
    }
}
