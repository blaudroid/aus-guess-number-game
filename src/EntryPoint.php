<?php
use Nastase\GuessNumber\Game;

require '../vendor/autoload.php';

$game = new Game();

try {
    $game->init();
} catch(Exception $e) {
    echo "Error while initializing the game: " . $e;
}
