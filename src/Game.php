<?php

namespace Nastase\GuessNumber;

use Exception;
use Nastase\GuessNumber\Engine\GameEngine;
use Nastase\GuessNumber\Engine\GameGUI;
use Nastase\GuessNumber\Services\RandomNumberGenerator;
use Nastase\GuessNumber\Strategy\ComputerStrategy;
use Nastase\GuessNumber\Strategy\Strategy;
use Nastase\GuessNumber\Strategy\UserStrategy;

class Game {
    private int                     $gameMode = self::GM_UNDEFINED;
    private array                   $availableGameModes = [self::GM_COMPUTER, self::GM_USER];

    private GameGUI                 $gameGUI;
    private GameEngine              $gameEngine;
    private RandomNumberGenerator   $randomNumberGenerator;

    public const                    GM_USER = 2;
    public const                    GM_COMPUTER = 1;
    public const                    GM_UNDEFINED = 0;

    /**
     * @throws Exception
     */
    public function init(): void
    {
        $this->gameGUI                  = new GameGUI();
        $this->gameEngine               = new GameEngine();
        $this->randomNumberGenerator    = new RandomNumberGenerator();

        $this->gameGUI->showMenu();

        match($this->gameGUI->getMenuSelection()) {
            GameGUI::START_GAME => $this->startGame(),
            GameGUI::CHOOSE_GAME_MODE => $this->selectGameMode(),
            GameGUI::QUIT_GAME => $this->exitGame(),
            default => $this->init()
        };
    }

    /**
     * @throws Exception
     */
    public function startGame(): void
    {
        if($this->gameMode === self::GM_UNDEFINED) {
            $this->gameGUI->printGameError('You cannot start a game before choosing a game mode!');
            $this->init();
        }

        $this->gameEngine->resetGame();
        $this->randomNumberGenerator->setRange(1, 100);
        $this->randomNumberGenerator->saveOriginalRange();

        $numberToBeGuessed = $this->randomNumberGenerator->generateRandomNumber();

        $strategy = $this->getGameStrategyBasedOnGameMode();
        $strategy->setNumberToGuess($numberToBeGuessed);
        $strategy->setRandomNumberGenerator($this->randomNumberGenerator);

        $this->gameEngine->setStrategy($strategy);

        $this->establishFinalRoundResult();

        $this->gameGUI->setFinishPageDetails([
            'numberGuessed' => $numberToBeGuessed,
            'triesCount' => $this->gameEngine->getTriesCount()
        ]);

        $this->gameGUI->showEndGameResult();

        $this->playAgain();
    }

    /**
     * @throws Exception
     */
    private function selectGameMode(): void
    {
        $this->gameGUI->showSelectGameModeMenu();

        $guiSelectedValue = $this->gameGUI->getMenuSelection();

        if(in_array($guiSelectedValue, $this->availableGameModes, true)) {
            $this->gameMode = $guiSelectedValue;
            $this->init();
        } else {
            if($guiSelectedValue === GameGUI::MENU_BACK){
                $this->init();
            }

            $this->gameGUI->printGameError('Unavailable game mode selected !');
            $this->gameGUI->showSelectGameModeMenu();
        }
    }

    private function getGameStrategyBasedOnGameMode(): Strategy
    {
        return match ($this->gameMode) {
            self::GM_COMPUTER => new ComputerStrategy(),
            default => new UserStrategy(),
        };
    }

    /**
     * @throws Exception
     */
    private function playAgain(): void
    {
        $this->gameGUI->showPlayAgainMenu();

        match($this->gameGUI->getMenuSelection()) {
            GameGUI::MENU_YES => $this->startGame(),
            GameGUI::MENU_NO => $this->init(),
            default => $this->playAgain()
        };
    }

    private function exitGame(): void
    {
        $this->gameGUI->printEndGameMessage();
        exit;
    }

    private function establishFinalRoundResult(): void
    {
        $this->gameGUI->printGuessNumberBetweenMessage($this->randomNumberGenerator->getOriginalMin(), $this->randomNumberGenerator->getOriginalMax());
        $this->gameEngine->playRound();

        if(!$this->gameEngine->getRoundStatus()) {
            $this->gameGUI->printRoundMessage($this->gameEngine->getRoundMessage());
            self::establishFinalRoundResult();
        }
    }

}
